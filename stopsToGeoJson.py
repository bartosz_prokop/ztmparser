import geojson
import parser

def main():
    p = parser.parse_ztm_file("przystankifull.txt")
    points = []
    for key in p:
        currStop = p.get(key)
        try:
            coordinates = (float(currStop.x.split()[1]), float(currStop.y.split()[1]))
        except IndexError:
            coordinates = (0.00, 0.00)
        points.append(geojson.Point(coordinates, properties={"street": currStop.street,
                                                             "id": currStop.id,
                                                             "direction": currStop.direction,
                                                             "lines": str(currStop.lines)}
                                    ))
    features = []
    print 'tu jestem'
    f = open('przystankiJSON.json', 'w')
    for p in points:
        feature = geojson.Feature(geometry=p)
        features.append(feature)
    collection = geojson.FeatureCollection(features)
    geojson.dump(collection, f, indent=4, sort_keys=True)
    f.close()
    return collection





if __name__ == "__main__":
    main()
