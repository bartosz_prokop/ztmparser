import re
import unittest
import codecs

# parser - zwraca dict przystankow


def parse_ztm_file(filename):
    f = codecs.open(filename, 'r', 'windows-1250')
    #f = open(filename, 'r')
    stops = {}
    while True:
        stopset = parse_stopset(f)
        if stopset is None:
            break
        parse_stops(f, stopset, stops)
        assert (f.readline().strip() == '#PR')
    return stops


def parse_stopset(f):
    headerline = f.readline()
    if headerline == '':
        return None
    numstopsline = f.readline()
    stopset = StopSet(build_stopset_data(headerline, numstopsline))
    return stopset


def build_stopset_data(header, numstops):
    data = get_stopset_data_from_line(header)
    data.append(get_number_of_stops_from_line(numstops))
    return data


def get_stopset_data_from_line(line):
    data = re.split('\s{2,}', line.strip())
    assert (len(data) == 4)
    return data


def get_number_of_stops_from_line(line):
    try:
        code, numstops = line.strip().split()
    except ValueError:
        numstops = '1'
    assert (code == '*PR')
    return numstops


def parse_stops(f, stopset, dest):
    for i in range(stopset.numstops):
        stop = Stop(get_stop_data_from_line(f.readline()))
        stop.add_stopset(stopset)
        stop.add_lines(get_line_list(f, stop))
        dest[stop.id] = stop

def get_stop_data_from_line(line):
    stopdata = re.split('\s{3,}', line.strip())
    assert (len(stopdata) == 6)
    return stopdata


def get_line_list(f, stop):
    linelist = []
    for i in range(stop.types):
        linelist += get_lines_from_line(f.readline())
    return linelist


def get_lines_from_line(line):
    regex = re.compile('\d+|[ZNELS]-*\d+|KML*\d*|WKD|ZS\d+')  # tu jest kwestia daszku ^ przy liniach, co on dokladnie oznacza? w opisie ztm ze rozne typy zatrzymania...
    lines = re.findall(regex, line.strip())
    assert (len(lines) == int(lines[0]) + 1)
    return lines[1:]


# helper classes


#jakie przystanki/linie analizujemy? tylko w warszawie? tutaj zwraca 5704 przystanki.... i co np jesli linia ma wiekszosc przystankow w wwie a np 3 poza...
#co z kierunkami przystankow... bedzie to mialo wplyw na grafy
#ktore linie dokladnie analizujemy? co z daszkami? nocnymi, zastepczymi, itd itp
#graf i hipergraf  reprezentacja
#co z hitting setem... algorytm


class StopSet:
    def __init__(self, args):
        self.id = args[0]
        self.name = args[1].encode('utf-8')
        self.citysymbol = args[2].encode('utf-8')
        self.cityname = args[3].encode('utf-8')
        self.numstops = int(args[4])


class Stop:
    def __init__(self, args):
        self.id = int(args[0])
        self.types = int(args[1])
        self.street = args[2].encode('utf-8')
        self.direction = args[3].encode('utf-8')
        self.y = args[4]
        self.x = args[5]
        self.lines = None
        self.stopset = None

    def add_stopset(self, stopset):
        self.stopset = stopset

    def add_lines(self, lines):
        self.lines = lines


def main():
    p = parse_ztm_file("przystankifull.txt")



class TestParser(unittest.TestCase):
    def test_get_lines_from_line(self):
        line = get_lines_from_line("13 101 E-1 L-1 L25 N01 KM1 KML S1 S3 WKD Z-1 Z32 ZS1")
        expected = ['101', 'E-1', 'L-1', 'L25', 'N01', 'KM1', 'KML', 'S1', 'S3', 'WKD', 'Z-1', 'Z32', 'ZS1']
        self.assertEqual(line, expected, '\nexpected:\t\t {0}\ngot:\t\t\t {1}'.format(expected, line))

if __name__ == "__main__":
    main()
